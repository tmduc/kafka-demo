package com.ducta.kafkademo.repositories;

import com.ducta.kafkademo.entities.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToDoRepository extends JpaRepository<ToDo, Long> {
}
