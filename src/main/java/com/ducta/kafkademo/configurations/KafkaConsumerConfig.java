package com.ducta.kafkademo.configurations;

import com.ducta.kafkademo.entities.ToDo;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {
    @Bean
    public ConsumerFactory<String, ToDo> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9096");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "group1");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(ToDo.class));
    }
    @Bean
    public ConsumerFactory<String, ToDo> persistentConsumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9097");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "group2");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), new JsonDeserializer<>(ToDo.class));
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ToDo> transformKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, ToDo> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ToDo> persistentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, ToDo> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(persistentConsumerFactory());
        return factory;
    }

}
