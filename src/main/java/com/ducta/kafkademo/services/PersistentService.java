package com.ducta.kafkademo.services;

import com.ducta.kafkademo.entities.ToDo;
import com.ducta.kafkademo.repositories.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PersistentService {

    @Autowired
    private ToDoRepository toDoRepository;

    @KafkaListener(topics = "ABC-standardized", groupId = "group1", containerFactory = "transformKafkaListenerContainerFactory")
    public void listen(ToDo toDo) {
        System.out.println("[Persistent service]: Received message: " + toDo.toString());
        System.out.println("[Persistent service]: Starting to write to Database");
        toDoRepository.save(toDo);
    }
}
