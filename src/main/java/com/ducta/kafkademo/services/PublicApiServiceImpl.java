package com.ducta.kafkademo.services;

import com.ducta.kafkademo.entities.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
public class PublicApiServiceImpl implements PublicApiService {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private KafkaTemplate<String, ToDo> toDoKafkaTemplate;

    @Override
    public void retrieveAndProduceData() {
        ResponseEntity<ToDo> responseEntity = restTemplate.getForEntity("https://jsonplaceholder.typicode.com/todos/1", ToDo.class);
        System.out.println("Fetched data from open API: " + Objects.requireNonNull(responseEntity.getBody()).toString());
        System.out.println("Starting to send raw data to ABC topic");
        ListenableFuture<SendResult<String, ToDo>> future = toDoKafkaTemplate.send("ABC", responseEntity.getBody());

        future.addCallback(new ListenableFutureCallback<SendResult<String, ToDo>>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("Unable to send message due to : " + throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, ToDo> result) {
                System.out.println("Sent message=[" + responseEntity.getBody().toString() + "] with offset=[" + result.getRecordMetadata()
                        .offset() + "]");
            }
        });
    }
}
