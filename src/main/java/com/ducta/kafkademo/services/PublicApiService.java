package com.ducta.kafkademo.services;

public interface PublicApiService {
    void retrieveAndProduceData();
}
