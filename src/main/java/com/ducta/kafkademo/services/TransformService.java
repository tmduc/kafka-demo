package com.ducta.kafkademo.services;

import com.ducta.kafkademo.entities.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class TransformService {

    @Autowired
    private KafkaTemplate<String, ToDo> toDoKafkaTemplate;


    @KafkaListener(topics = "ABC", groupId = "group1", containerFactory = "transformKafkaListenerContainerFactory")
    public void listen(ToDo toDo) {
        System.out.println("Received message in group1: " + toDo.toString());
        System.out.println("Starting to transform and send data to ABC Standardize Topic");
        toDo.setCompleted(true);
        toDo.setTitle("This task has been completed");
        ListenableFuture<SendResult<String, ToDo>> future = toDoKafkaTemplate.send("ABC-standardized", toDo);
        future.addCallback(new ListenableFutureCallback<SendResult<String, ToDo>>() {
            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("Unable to send message due to : " + throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, ToDo> result) {
                System.out.println("Sent message=[" + toDo.toString() + "] with offset=[" + result.getRecordMetadata()
                        .offset() + "]");
            }
        });
    }
}
